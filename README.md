# Bible Reference

Bible Reference is a Drupal module to make possible to manage and display Bible references within Drupal websites.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Add your firs Bible reference field to any of your content type.
