/**
 * @file
 * Defines JavaScript behaviors for the complex widget.
 */
(function ($, Drupal, drupalSettings) {
  const initedClass = 'bibleref-widget-inited';
  const widgetSelector = '.container-bibleref-widget';
  const bookFieldSelector = '[data-bibleref-property="book"]';
  const chapterFieldSelector = '[data-bibleref-property="chapter"]';
  const verseFieldSelector = '[data-bibleref-property="verse"]';
  const fromChapterFieldSelector = chapterFieldSelector + '[data-bibleref-part="from"]';
  const toChapterFieldSelector = chapterFieldSelector + '[data-bibleref-part="to"]';

  function initBibleref(index, el) {
    const $item = $(el);
    setSubtreeForRow(el);

    $item.find(bookFieldSelector).trigger('change');
    $item.find(chapterFieldSelector).trigger('change');
  }

  function getChaptersOfRow(el) {
    const $widget = $(el).parents(widgetSelector);
    return $widget.find(chapterFieldSelector);
  }

  function getFromChaptersOfRow(el) {
    const $widget = $(el).parents(widgetSelector);
    return $widget.find(fromChapterFieldSelector);
  }

  function getVersesOfRow(el, all) {
    const $widget = $(el).parents(widgetSelector);
    const $verses = $widget.find(verseFieldSelector);
    if (all || !isInComplexRow(el)) {
      return $verses;
    }
    const part = $(el).attr('data-bibleref-part');
    return $verses.filter(`[data-bibleref-part="${part}"]`);
  }

  function setSubtreeForRow(el) {
    const $widget = $(el).parents(widgetSelector);
    const $book = $widget.find(bookFieldSelector);
    if ($book.length) {
      const subtree = getSubTree($book.val());
      $widget.data('tree', subtree);
      $widget.data('max-chapter', getMaxChapter(subtree));
    }
  }

  function getMaxChapter(subtree) {
    const items = Object.keys(subtree);
    if (!items.length) {
      return 150;
    }

    const keys = items.map(function (i) {
      return parseInt(i);
    });
    return Math.max(...keys);
  }

  function getMaxVerse(subtree, chapter) {
    return subtree[chapter] || 200;
  }

  function getSubtreeForRow(el) {
    const $widget = $(el).parents(widgetSelector);
    const $book = $widget.find(bookFieldSelector);
    return getSubTree($book.val());
  }

  function getSubTree(tid) {
    return drupalSettings.biblerefTree[tid] ?? {};
  }

  function isInComplexRow(el) {
    const $widget = $(el).parents(widgetSelector);
    return Boolean(parseInt($widget.attr('data-bibleref-complex')));
  }

  Drupal.behaviors.biblerefWidget = {
    attach(context) {
      const $context = $(context);
      $(`body:not(.${initedClass})`)
        .addClass(initedClass)
        .on('change', `${widgetSelector} ${bookFieldSelector}`, function onBookChanged(ev) {
          const subtree = getSubtreeForRow(this);
          const $chapters = getChaptersOfRow(this);
          $chapters.attr('max', getMaxChapter(subtree));
        })
        .on('change', `${widgetSelector} ${chapterFieldSelector}`, function onChapterChanged(ev) {
          const $el = $(this);
          const subtree = getSubtreeForRow(this);
          const $verses = getVersesOfRow(this);
          $verses.attr('max', getMaxVerse(subtree, $el.val()));
        })
        .on('focus', toChapterFieldSelector, function onToChapterFocus(ev) {
          if ($(this).val()) {
            return;
          }
          const $fromChapter = getFromChaptersOfRow(this);
          $(this).val($fromChapter.val());
        });

      $context
        .find(`.container-bibleref-widget:not(.${initedClass})`)
        .addClass(initedClass)
        .each(initBibleref);
    }
  };
})(jQuery, Drupal, drupalSettings);
