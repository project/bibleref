<?php

/**
 * @file
 * Hooks specific to the Bibleref module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter data before formatting a reference.
 *
 * @param array $args
 *   Arguments to alter.
 * @param array $context
 *   Context.
 *
 * @see \Drupal\bibleref\Formatter\BibleRefFormatter::format()
 */
function hook_bibleref_format_data(array &$args, array &$context) {
  // Example implementation.
  if ($args['format'] === ':book :chapter.:verse') {
    // Do not display verse.
    $args['format'] = ':book :chapter';
  }
}

/**
 * Alter data before formatting verse part of reference.
 *
 * @param array $args
 *   Arguments to alter.
 * @param array $context
 *   Context.
 *
 * @see \Drupal\bibleref\Formatter\BibleRefFormatter::formatVerse()
 */
function hook_bibleref_format_verse(array &$args, array &$context) {
  // Example implementation.
  // Display only verse_from; Never display verse_to and additional verses.
  $args['format'] = ':verse_from';
}

/**
 * @} End of "addtogroup hooks".
 */
