<?php

namespace Drupal\bibleref\Field;

use Drupal\Core\Field\FieldItemList;

/**
 * Bible verse reference field item list.
 *
 * @package Drupal\bible_reference\Field
 */
class BibleReferenceFieldItemList extends FieldItemList {

}
