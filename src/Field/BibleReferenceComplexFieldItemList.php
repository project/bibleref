<?php

namespace Drupal\bibleref\Field;

/**
 * Bible verse complex reference field item list.
 *
 * @package Drupal\bible_reference\Field
 */
class BibleReferenceComplexFieldItemList extends BibleReferenceFieldItemList {

}
