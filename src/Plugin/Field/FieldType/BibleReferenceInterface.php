<?php

namespace Drupal\bibleref\Plugin\Field\FieldType;

/**
 * Bible verse reference interface.
 *
 * @package Drupal\bibleref\Plugin\Field\FieldType
 */
interface BibleReferenceInterface {

  const PROPERTY_BOOK = 'book';
  const PROPERTY_CHAPTER = 'chapter';
  const PROPERTY_VERSE_FROM = 'verse_from';
  const PROPERTY_VERSE_TO = 'verse_to';
  const PROPERTY_VERSE_ADDITIONAL = 'verse_additional';

  /**
   * Get referenced book term.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   Referenced book term.
   */
  public function getBook();

  /**
   * Get number of referenced book.
   *
   * @return int
   *   Chapter number.
   */
  public function getChapter();

  /**
   * Get verse from value.
   *
   * @return int|null
   *   Verse from value.
   */
  public function getVerseFrom();

  /**
   * Get verse to value.
   *
   * @return int|null
   *   Verse from value.
   */
  public function getVerseTo();

  /**
   * Get verse string.
   *
   * @return \Drupal\Component\Render\MarkupInterface|string
   *   Markup.
   */
  public function getVerse();

  /**
   * Get additional reference.
   *
   * @return \Drupal\Component\Render\MarkupInterface|string|null
   *   Markup.
   */
  public function getAdditional();

  /**
   * Get values as array.
   *
   * @return array
   *   Values array.
   */
  public function toArray();

  /**
   * Get value as string.
   *
   * @return string
   *   Formatted reference.
   */
  public function toString();

}
