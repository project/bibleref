<?php

namespace Drupal\bibleref\Plugin\Field\FieldType;

/**
 * Bible verse reference interface.
 *
 * @package Drupal\bibleref\Plugin\Field\FieldType
 */
interface BibleReferenceComplexInterface extends BibleReferenceInterface {

  const PROPERTY_CHAPTER_TO = 'chapter_to';

  /**
   * Get reference chapter from.
   *
   * @return int
   *   Chapter number.
   */
  public function getChapterFrom();

  /**
   * Get reference chapter to.
   *
   * @return int
   *   Chapter number.
   */
  public function getChapterTo();

}
