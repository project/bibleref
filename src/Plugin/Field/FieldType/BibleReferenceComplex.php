<?php

namespace Drupal\bibleref\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the `bible_reference` field type.
 *
 * @FieldType(
 *   id = "bible_reference_complex",
 *   label = @Translation("Bible verse reference - Complex"),
 *   description = @Translation("An entity field containing a Bible verse reference"),
 *   category = "reference",
 *   default_widget = "bible_reference_complex_widget",
 *   default_formatter = "bible_reference_complex_formatter_default",
 *   list_class = "\Drupal\bibleref\Field\BibleReferenceComplexFieldItemList"
 * )
 */
class BibleReferenceComplex extends BibleReference implements BibleReferenceComplexInterface {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties[self::PROPERTY_CHAPTER_TO] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Chapter to', [], ['context' => 'bibleref']))
      ->setSetting('unsigned', TRUE)
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    return [
      'columns' => [
        self::PROPERTY_BOOK => $schema['columns'][self::PROPERTY_BOOK],
        self::PROPERTY_CHAPTER => $schema['columns'][self::PROPERTY_CHAPTER],
        self::PROPERTY_VERSE_FROM => $schema['columns'][self::PROPERTY_VERSE_FROM],
        self::PROPERTY_CHAPTER_TO => $schema['columns'][self::PROPERTY_CHAPTER],
        self::PROPERTY_VERSE_TO => $schema['columns'][self::PROPERTY_VERSE_TO],
        self::PROPERTY_VERSE_ADDITIONAL => $schema['columns'][self::PROPERTY_VERSE_ADDITIONAL],
      ],
      'indexes' => [
        'book_id' => [self::PROPERTY_BOOK],
        'ref' => [
          self::PROPERTY_BOOK,
          self::PROPERTY_CHAPTER,
          self::PROPERTY_VERSE_FROM,
          self::PROPERTY_CHAPTER_TO,
          self::PROPERTY_VERSE_TO,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getChapter() {
    return $this->getChapterFrom();
  }

  /**
   * {@inheritdoc}
   */
  public function getChapterFrom() {
    return $this->values[self::PROPERTY_CHAPTER];
  }

  /**
   * {@inheritdoc}
   */
  public function getChapterTo() {
    return $this->values[self::PROPERTY_CHAPTER_TO];
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return $this->values + [
      self::PROPERTY_BOOK => NULL,
      self::PROPERTY_CHAPTER => NULL,
      self::PROPERTY_VERSE_FROM => NULL,
      self::PROPERTY_CHAPTER_TO => NULL,
      self::PROPERTY_VERSE_TO => NULL,
      self::PROPERTY_VERSE_ADDITIONAL => NULL,
    ];
  }

}
