<?php

namespace Drupal\bibleref\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;

/**
 * Defines the `bible_reference` field type.
 *
 * @FieldType(
 *   id = "bible_reference",
 *   label = @Translation("Bible verse reference - Simple"),
 *   description = @Translation("An entity field containing a Bible verse reference"),
 *   category = "reference",
 *   default_widget = "bible_reference_widget_default",
 *   default_formatter = "bible_reference_formatter_default",
 *   list_class = "\Drupal\bibleref\Field\BibleReferenceFieldItemList"
 * )
 */
class BibleReference extends FieldItemBase implements BibleReferenceInterface {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties[self::PROPERTY_BOOK] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Book', [], ['context' => 'bibleref']))
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE);

    $target_type_info = \Drupal::entityTypeManager()->getDefinition('taxonomy_term');
    $properties['entity'] = DataReferenceDefinition::create('entity')
      ->setLabel($target_type_info->getLabel())
      ->setDescription(new TranslatableMarkup('The referenced entity'))
      // The entity object is computed out of the entity ID.
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create('taxonomy_term'))
      // We can add a constraint for the target entity type. The list of
      // referenceable bundles is a field setting, so the corresponding
      // constraint is added dynamically in ::getConstraints().
      ->addConstraint('EntityType', 'taxonomy_term');

    $properties[self::PROPERTY_CHAPTER] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Chapter', [], ['context' => 'bibleref']))
      ->setSetting('unsigned', TRUE)
      ->setRequired(FALSE);

    $properties[self::PROPERTY_VERSE_FROM] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('From verse', [], ['context' => 'bibleref']))
      ->setRequired(FALSE)
      ->setSetting('unsigned', TRUE);

    $properties[self::PROPERTY_VERSE_TO] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('To verse', [], ['context' => 'bibleref']))
      ->setRequired(FALSE)
      ->setSetting('unsigned', TRUE);

    $properties[self::PROPERTY_VERSE_ADDITIONAL] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Additional verses', [], ['context' => 'bibleref']))
      ->setRequired(FALSE)
      ->setSetting('case_sensitive', FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        self::PROPERTY_BOOK => [
          'description' => 'The ID of the target book term.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        self::PROPERTY_CHAPTER => [
          'description' => 'Chapter number.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        self::PROPERTY_VERSE_FROM => [
          'description' => 'First referenced verse.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        self::PROPERTY_VERSE_TO => [
          'description' => 'Last referenced verse.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        self::PROPERTY_VERSE_ADDITIONAL => [
          'type' => 'varchar',
          'length' => 50,
          'binary' => FALSE,
        ],
      ],
      'indexes' => [
        'book_id' => [self::PROPERTY_BOOK],
        'ref' => [
          self::PROPERTY_BOOK,
          self::PROPERTY_CHAPTER,
          self::PROPERTY_VERSE_FROM,
          self::PROPERTY_VERSE_TO,
        ],
      ],
    ];
  }

  /**
   * Retrieves a service from the container.
   *
   * @param string $id
   *   The ID of the service to retrieve.
   *
   * @return mixed
   *   The specified service.
   */
  protected function getService($id) {
    return \Drupal::service($id);
  }

  /**
   * Load book term.
   *
   * @param int $book_id
   *   Book term id.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Book term.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadEntity($book_id) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->getService('entity_type.manager');
    /** @var \Drupal\taxonomy\TermStorageInterface $storage */
    $storage = $entity_type_manager->getStorage('taxonomy_term');
    return $storage->load($book_id);
  }

  /**
   * Get formatter service.
   *
   * @return \Drupal\bibleref\Formatter\BibleRefFormatterInterface
   *   Formatter service.
   */
  protected function getFormatter() {
    return $this->getService('bibleref.formater');
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if (!empty($values[self::PROPERTY_BOOK])) {
      $values['entity'] = $this->loadEntity($values[self::PROPERTY_BOOK]);
    }
    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function getBook() {
    return $this->properties['entity']->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getChapter() {
    return $this->values[self::PROPERTY_CHAPTER];
  }

  /**
   * {@inheritdoc}
   */
  public function getAdditional() {
    return $this->values[self::PROPERTY_VERSE_ADDITIONAL] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getVerseFrom() {
    return $this->values[self::PROPERTY_VERSE_FROM] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getVerseTo() {
    return $this->values[self::PROPERTY_VERSE_TO] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getVerse() {
    return $this->getFormatter()->formatVerse($this);
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return $this->values + [
      self::PROPERTY_BOOK => NULL,
      self::PROPERTY_CHAPTER => NULL,
      self::PROPERTY_VERSE_FROM => NULL,
      self::PROPERTY_VERSE_TO => NULL,
      self::PROPERTY_VERSE_ADDITIONAL => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function toString() {
    return $this->getFormatter()->format($this);
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    $settings = parent::getSettings();
    $settings['target_type'] = 'taxonomy_term';
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSetting($setting_name) {
    if ($setting_name === 'target_type') {
      return 'taxonomy_term';
    }
    return $this->getFieldDefinition()->getSetting($setting_name);
  }

}
