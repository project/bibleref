<?php

namespace Drupal\bibleref\Plugin\Field\FieldWidget;

use Drupal\bibleref\Plugin\Field\FieldType\BibleReference;
use Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "bible_reference_widget_default",
 *   label = @Translation("Bible verse default widget"),
 *   field_types = {
 *     "bible_reference"
 *   }
 * )
 */
class BibleReferenceDefaultWidget extends OptionsWidgetBase implements ContainerFactoryPluginInterface {

  const GRANULARITY_BOOK = 1;
  const GRANULARITY_CHAPTER = 2;
  const GRANULARITY_VERSE_FROM = 4;
  const GRANULARITY_VERSE_TO = 8;

  /**
   * Term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Current language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected LanguageInterface $currentLang;

  /**
   * Book/chapter/max verse tree.
   *
   * @var array
   */
  protected array $tree;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
      $container->get('module_handler'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    TermStorageInterface $term_storage,
    ModuleHandlerInterface $module_handler,
    LanguageManagerInterface $language_manager
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings
    );
    $this->column = BibleReference::PROPERTY_BOOK;
    $this->termStorage = $term_storage;
    $this->moduleHandler = $module_handler;
    $this->currentLang = $language_manager->getCurrentLanguage();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $item = $items[$delta] ?? NULL;
    $value = $this->getFormValues($item);

    // We need to check against a flat list of options.
    $flat_options = OptGroup::flattenOptions($this->getOptions($items->getEntity()));
    $required = $this->fieldDefinition->isRequired();

    $element[BibleReference::PROPERTY_BOOK] = parent::formElement($items, $delta, $element, $form, $form_state) + [
      '#type' => 'select',
      '#options' => $this->getOptions($items->getEntity()),
      '#default_value' => isset($flat_options[$value[BibleReference::PROPERTY_BOOK]]) ? $value[BibleReference::PROPERTY_BOOK] : NULL,
      // Do not display a 'multiple' select box if there is only one option.
      '#multiple' => FALSE,
      '#required' => $required,
      '#weight' => 0,
    ];
    $element[BibleReference::PROPERTY_BOOK]['#attributes']['data-bibleref-property'] = 'book';
    $element[BibleReference::PROPERTY_BOOK]['#weight'] = 0;
    $element[BibleReference::PROPERTY_BOOK]['#title'] = $this->t('Book', [], ['context' => 'bibleref']);
    unset($element[BibleReference::PROPERTY_BOOK]['#title_display']);
    $element[BibleReference::PROPERTY_BOOK]['#states'] = $this->getStates(BibleReference::PROPERTY_BOOK, $delta);

    $element[BibleReference::PROPERTY_CHAPTER] = [
      '#title' => $this->t('Chapter', [], ['context' => 'bibleref']),
      '#type' => 'number',
      '#default_value' => $value[BibleReference::PROPERTY_CHAPTER],
      '#weight' => 1,
      '#min' => 1,
      '#max' => 999,
      '#states' => $this->getStates(BibleReference::PROPERTY_CHAPTER, $delta),
      '#attributes' => [
        'data-bibleref-property' => 'chapter',
        'data-bibleref-part' => 'from',
      ],
    ];
    $element[BibleReference::PROPERTY_VERSE_FROM] = [
      '#title' => $this->t('Verse from', [], ['context' => 'bibleref']),
      '#type' => 'number',
      '#default_value' => $value[BibleReference::PROPERTY_VERSE_FROM],
      '#weight' => 2,
      '#size' => 3,
      '#min' => 0,
      '#max' => 999,
      '#states' => $this->getStates(BibleReference::PROPERTY_VERSE_FROM, $delta),
      '#attributes' => [
        'data-bibleref-property' => 'verse',
        'data-bibleref-part' => 'from',
      ],
    ];
    $element[BibleReference::PROPERTY_VERSE_TO] = [
      '#title' => $this->t('to', [], ['context' => 'bibleref']),
      '#type' => 'number',
      '#default_value' => $value[BibleReference::PROPERTY_VERSE_TO],
      '#weight' => 3,
      '#size' => 3,
      '#min' => 0,
      '#max' => 999,
      '#states' => $this->getStates(BibleReference::PROPERTY_VERSE_TO, $delta),
      '#attributes' => [
        'data-bibleref-property' => 'verse',
        'data-bibleref-part' => 'to',
      ],
    ];
    if ($this->getSetting('additional')) {
      $element[BibleReference::PROPERTY_VERSE_ADDITIONAL] = [
        '#title' => $this->t('additional', [], ['context' => 'bibleref']),
        '#type' => 'textfield',
        '#required' => FALSE,
        '#default_value' => $value[BibleReference::PROPERTY_VERSE_ADDITIONAL],
        '#weight' => 4,
        '#size' => 15,
        '#maxlength' => 50,
        '#states' => $this->getStates(BibleReference::PROPERTY_VERSE_ADDITIONAL, $delta),
        '#attributes' => [
          'data-bibleref-property' => 'additional',
        ],
      ];
    }

    $element['#theme_wrappers'][] = 'container';
    $element['#attributes']['class'][] = 'container-bibleref-widget';
    $element['#attached']['library'][] = 'bibleref/bibleref-widget';
    $element['#attached']['drupalSettings']['biblerefTree'] = $this->tree;
    return $element;
  }

  /**
   * Get states for field.
   *
   * @param string $property
   *   Property name.
   * @param int $delta
   *   Item delta.
   *
   * @return array
   *   States definition.
   */
  protected function getStates($property, $delta): array {
    $required = $this->fieldDefinition->isRequired();
    $granularity = $this->getSetting('required_granularity');
    $required_base = [
      $this->stateInput($delta, BibleReference::PROPERTY_BOOK) => ['!value' => '_none'],
    ];

    $states = [];
    if ($property === BibleReference::PROPERTY_VERSE_ADDITIONAL) {
      $states['disabled'][$this->stateInput($delta, BibleReference::PROPERTY_BOOK)] = ['value' => '_none'];
    }
    elseif ($property === BibleReference::PROPERTY_VERSE_TO) {
      $states['disabled'][$this->stateInput($delta, BibleReference::PROPERTY_VERSE_FROM)] = ['empty' => TRUE];
      if ($required && $granularity >= self::GRANULARITY_VERSE_TO) {
        $states['required'] = $required_base;
      }
    }
    elseif ($property === BibleReference::PROPERTY_VERSE_FROM) {
      $states['disabled'][$this->stateInput($delta, BibleReference::PROPERTY_CHAPTER)] = ['empty' => TRUE];
      if ($required && $granularity >= self::GRANULARITY_VERSE_FROM) {
        $states['required'] = $required_base;
      }
    }
    elseif ($property === BibleReference::PROPERTY_CHAPTER) {
      $states['disabled'][$this->stateInput($delta, BibleReference::PROPERTY_BOOK)] = ['value' => '_none'];
      if ($required && $granularity >= self::GRANULARITY_CHAPTER) {
        $states['required'] = $required_base;
      }
    }
    return $states;
  }

  /**
   * Build input reference for state definition.
   *
   * @param int $delta
   *   Item delta value.
   * @param string $field
   *   Field name.
   *
   * @return string
   *   Input reference for state definition.
   */
  protected function stateInput($delta, $field) {
    $name = $this->fieldDefinition->getName();
    return ':input[name="' . $name . '[' . $delta . '][' . $field . ']"]';
  }

  /**
   * Get form values.
   *
   * @param \Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface|null $item
   *   Field item.
   *
   * @return array
   *   Form values.
   */
  protected function getFormValues(BibleReferenceInterface $item = NULL) {
    $value = [];
    if (isset($item)) {
      $array = $item->toArray();
      $value += [
        BibleReference::PROPERTY_BOOK => $array[BibleReference::PROPERTY_BOOK],
        BibleReference::PROPERTY_CHAPTER => $array[BibleReference::PROPERTY_CHAPTER],
        BibleReference::PROPERTY_VERSE_FROM => $array[BibleReference::PROPERTY_VERSE_FROM],
        BibleReference::PROPERTY_VERSE_TO => $array[BibleReference::PROPERTY_VERSE_TO],
        BibleReference::PROPERTY_VERSE_ADDITIONAL => $array[BibleReference::PROPERTY_VERSE_ADDITIONAL],
      ];
    }
    $value += [
      BibleReference::PROPERTY_BOOK => NULL,
      BibleReference::PROPERTY_CHAPTER => NULL,
      BibleReference::PROPERTY_VERSE_FROM => NULL,
      BibleReference::PROPERTY_VERSE_TO => NULL,
      BibleReference::PROPERTY_VERSE_ADDITIONAL => NULL,
    ];
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    if (!isset($this->options)) {
      /** @var \Drupal\taxonomy\TermInterface[] $terms */
      $terms = $this->termStorage->loadTree('bibleref_books', 0, 1, TRUE);
      $this->options = $this->buildOptionsFromTermList($terms, $entity);
    }
    return $this->options;
  }

  /**
   * Build options from term list.
   *
   * @param \Drupal\taxonomy\TermInterface[] $terms
   *   Term list.
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function buildOptionsFromTermList(
    array $terms,
    FieldableEntityInterface $entity
  ) {
    $options = [
      '_none' => $this->t('- Select a value -'),
    ];
    $langcode = $this->currentLang->getId();
    $tree = [];
    foreach ($terms as $term) {
      $label = $term->label();
      if ($term->hasTranslation($langcode)) {
        $label = $term->getTranslation($langcode)->label();
      }
      $options[$term->id()] = $label;

      $chapters = (int) $term->get('field_chapters')->getString();
      $tree[$term->id()] = array_fill(1, max(1, $chapters), []);
      if (!$term->get('field_verses')->isEmpty()) {
        $data = json_decode($term->get('field_verses')->getString(), TRUE);
        foreach ($data as $item) {
          $tree[$term->id()][$item['chapter']] = $item['verses'];
        }
      }
    }

    $this->tree = $tree;

    $context = [
      'fieldDefinition' => $this->fieldDefinition,
      'entity' => $entity,
    ];
    $this->moduleHandler->alter('options_list', $options, $context);

    array_walk_recursive($options, [$this, 'sanitizeLabel']);

    // Options might be nested ("optgroups"). If the widget does not support
    // nested options, flatten the list.
    if (!$this->supportsGroups()) {
      $options = OptGroup::flattenOptions($options);
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $return = [];
    foreach ($values as $item) {
      if (empty($item['book'][0]['book'])) {
        continue;
      }

      $raw = [
        BibleReference::PROPERTY_BOOK => $item['book'][0]['book'],
        BibleReference::PROPERTY_CHAPTER => $item[BibleReference::PROPERTY_CHAPTER],
        BibleReference::PROPERTY_VERSE_FROM => $item[BibleReference::PROPERTY_VERSE_FROM],
        BibleReference::PROPERTY_VERSE_TO => $item[BibleReference::PROPERTY_VERSE_TO],
        BibleReference::PROPERTY_VERSE_ADDITIONAL => NULL,
      ];
      if (!empty($item[BibleReference::PROPERTY_VERSE_ADDITIONAL])) {
        $raw[BibleReference::PROPERTY_VERSE_ADDITIONAL] = $item[BibleReference::PROPERTY_VERSE_ADDITIONAL];
      }
      $return[] = $this->castValue($raw);
    }

    return $return;
  }

  /**
   * Cast property values.
   *
   * @param array $raw
   *   Raw values.
   *
   * @return array
   *   Casted values.
   */
  protected function castValue(array $raw): array {
    $int_properties = [
      BibleReference::PROPERTY_BOOK,
      BibleReference::PROPERTY_CHAPTER,
      BibleReference::PROPERTY_VERSE_FROM,
      BibleReference::PROPERTY_VERSE_TO,
    ];

    $values = [];
    foreach ($raw as $property => $value) {
      if (empty($value)) {
        $values[$property] = NULL;
      }
      elseif (in_array($property, $int_properties)) {
        $values[$property] = (int) $value;
      }
      else {
        $values[$property] = $value;
      }
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['required_granularity'] = self::GRANULARITY_VERSE_FROM;
    $settings['additional'] = TRUE;
    return $settings;
  }

  /**
   * Granularity options.
   *
   * @return array
   *   Granularity options.
   */
  protected function getGranularityList(): array {
    return [
      self::GRANULARITY_BOOK => $this->t('Book', [], ['context' => 'bibleref']),
      self::GRANULARITY_CHAPTER => $this->t('Book, Chapter', [], ['context' => 'bibleref']),
      self::GRANULARITY_VERSE_FROM => $this->t('Book, Chapter, Verse from', [], ['context' => 'bibleref']),
      self::GRANULARITY_VERSE_TO => $this->t('Book, Chapter, Verse from, Verse to', [], ['context' => 'bibleref']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['required_granularity'] = [
      '#type' => 'select',
      '#title' => $this->t('Required granularity', [], ['context' => 'bibleref']),
      '#default_value' => $this->getSetting('required_granularity'),
      '#options' => $this->getGranularityList(),
    ];
    $element['additional'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Additional field', [], ['context' => 'bibleref']),
      '#default_value' => $this->getSetting('additional'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $granularity = $this->getSetting('required_granularity');
    $summary[] = $this->t('Granularity: @granularity',
      ['@granularity' => $this->getGranularityList()[$granularity]],
      ['context' => 'bibleref']
    );
    if ($this->getSetting('additional')) {
      $summary[] = $this->t('With additional references', [], ['context' => 'bibleref']);
    }
    return $summary;
  }

}
