<?php

namespace Drupal\bibleref\Plugin\Field\FieldWidget;

use Drupal\bibleref\Plugin\Field\FieldType\BibleReference;
use Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceComplexInterface;
use Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "bible_reference_complex_widget",
 *   label = @Translation("Bible verse complex widget"),
 *   field_types = {
 *     "bible_reference_complex"
 *   }
 * )
 */
class BibleReferenceComplexWidget extends BibleReferenceDefaultWidget implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $item = $items[$delta] ?? NULL;
    $value = $this->getFormValues($item);
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element[BibleReferenceComplexInterface::PROPERTY_BOOK]['#weight'] = 0;
    $element[BibleReferenceComplexInterface::PROPERTY_CHAPTER]['#weight'] = 1;
    $element[BibleReferenceComplexInterface::PROPERTY_VERSE_FROM]['#weight'] = 2;
    $element[BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO] = [
      '#title' => $this->t('Chapter to', [], ['context' => 'bibleref']),
      '#type' => 'number',
      '#default_value' => $value[BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO],
      '#weight' => 3,
      '#min' => 1,
      '#max' => 999,
      '#states' => $this->getStates(BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO, $delta),
      '#attributes' => [
        'data-bibleref-property' => 'chapter',
        'data-bibleref-part' => 'to',
      ],
    ];
    $element[BibleReferenceComplexInterface::PROPERTY_VERSE_TO]['#weight'] = 4;
    if ($this->getSetting('additional')) {
      $element[BibleReferenceComplexInterface::PROPERTY_VERSE_ADDITIONAL]['#weight'] = 5;
    }
    $element['#attributes']['data-bibleref-complex'] = 1;
    return $element;
  }

  /**
   * Get states for field.
   *
   * @param string $property
   *   Property name.
   * @param int $delta
   *   Item delta.
   *
   * @return array
   *   States definition.
   */
  protected function getStates($property, $delta): array {
    $required = $this->fieldDefinition->isRequired();
    $granularity = $this->getSetting('required_granularity');
    $required_base = [
      $this->stateInput($delta, BibleReference::PROPERTY_BOOK) => ['!value' => '_none'],
    ];

    $states = [];
    if ($property === BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO) {
      $states['disabled'][$this->stateInput($delta, BibleReferenceComplexInterface::PROPERTY_VERSE_FROM)] = [
        'empty' => TRUE,
      ];
      if ($required && $granularity >= self::GRANULARITY_VERSE_TO) {
        $states['required'] = $required_base;
      }
    }
    elseif ($property === BibleReference::PROPERTY_VERSE_TO) {
      $states['disabled'][$this->stateInput($delta, BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO)] = [
        'empty' => TRUE,
      ];
      if ($required && $granularity >= self::GRANULARITY_VERSE_TO) {
        $states['required'] = $required_base;
      }
    }
    else {
      $states = parent::getStates($property, $delta);
    }
    return $states;
  }

  /**
   * Build input reference for state definition.
   *
   * @param int $delta
   *   Item delta value.
   * @param string $field
   *   Field name.
   *
   * @return string
   *   Input reference for state definition.
   */
  protected function stateInput($delta, $field) {
    $name = $this->fieldDefinition->getName();
    return ':input[name="' . $name . '[' . $delta . '][' . $field . ']"]';
  }

  /**
   * Get form values.
   *
   * @param \Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface|null $item
   *   Field item.
   *
   * @return array
   *   Form values.
   */
  protected function getFormValues(BibleReferenceInterface $item = NULL) {
    $value = [];
    if (isset($item)) {
      $array = $item->toArray();
      $value += [
        BibleReferenceComplexInterface::PROPERTY_BOOK => $array[BibleReferenceComplexInterface::PROPERTY_BOOK],
        BibleReferenceComplexInterface::PROPERTY_CHAPTER => $array[BibleReferenceComplexInterface::PROPERTY_CHAPTER],
        BibleReferenceComplexInterface::PROPERTY_VERSE_FROM => $array[BibleReferenceComplexInterface::PROPERTY_VERSE_FROM],
        BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO => $array[BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO],
        BibleReferenceComplexInterface::PROPERTY_VERSE_TO => $array[BibleReferenceComplexInterface::PROPERTY_VERSE_TO],
        BibleReferenceComplexInterface::PROPERTY_VERSE_ADDITIONAL => $array[BibleReferenceComplexInterface::PROPERTY_VERSE_ADDITIONAL],
      ];
    }
    $value += [
      BibleReferenceComplexInterface::PROPERTY_BOOK => NULL,
      BibleReferenceComplexInterface::PROPERTY_CHAPTER => NULL,
      BibleReferenceComplexInterface::PROPERTY_VERSE_FROM => NULL,
      BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO => NULL,
      BibleReferenceComplexInterface::PROPERTY_VERSE_TO => NULL,
      BibleReferenceComplexInterface::PROPERTY_VERSE_ADDITIONAL => NULL,
    ];
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $return = [];
    foreach ($values as $item) {
      if (empty($item['book'][0]['book'])) {
        continue;
      }

      $raw = [
        BibleReferenceComplexInterface::PROPERTY_BOOK => $item['book'][0]['book'],
        BibleReferenceComplexInterface::PROPERTY_CHAPTER => $item[BibleReferenceComplexInterface::PROPERTY_CHAPTER],
        BibleReferenceComplexInterface::PROPERTY_VERSE_FROM => $item[BibleReferenceComplexInterface::PROPERTY_VERSE_FROM],
        BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO => $item[BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO],
        BibleReferenceComplexInterface::PROPERTY_VERSE_TO => $item[BibleReferenceComplexInterface::PROPERTY_VERSE_TO],
        BibleReferenceComplexInterface::PROPERTY_VERSE_ADDITIONAL => NULL,
      ];
      if (!empty($item[BibleReferenceComplexInterface::PROPERTY_VERSE_ADDITIONAL])) {
        $raw[BibleReferenceComplexInterface::PROPERTY_VERSE_ADDITIONAL] = $item[BibleReferenceComplexInterface::PROPERTY_VERSE_ADDITIONAL];
      }
      $return[] = $this->castValue($raw);
    }

    return $return;
  }

  /**
   * Cast property values.
   *
   * @param array $raw
   *   Raw values.
   *
   * @return array
   *   Casted values.
   */
  protected function castValue(array $raw): array {
    $int_properties = [
      BibleReferenceComplexInterface::PROPERTY_BOOK,
      BibleReferenceComplexInterface::PROPERTY_CHAPTER,
      BibleReferenceComplexInterface::PROPERTY_VERSE_FROM,
      BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO,
      BibleReferenceComplexInterface::PROPERTY_VERSE_TO,
    ];

    $values = [];
    foreach ($raw as $property => $value) {
      if (empty($value)) {
        $values[$property] = NULL;
      }
      elseif (in_array($property, $int_properties)) {
        $values[$property] = (int) $value;
      }
      else {
        $values[$property] = $value;
      }
    }

    return $values;
  }

}
