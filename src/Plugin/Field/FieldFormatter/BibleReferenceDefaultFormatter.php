<?php

namespace Drupal\bibleref\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'chronology_date_formatter_default' formatter.
 *
 * @FieldFormatter(
 *   id = "bible_reference_formatter_default",
 *   label = @Translation("Bible reference default formatter"),
 *   description = @Translation("Bible reference default formatter"),
 *   field_types = {
 *     "bible_reference"
 *   }
 * )
 */
class BibleReferenceDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      /** @var \Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface $item */
      $elements[$delta] = [
        '#markup' => $item->toString(),
        '#cache' => [
          'contexts' => ['languages'],
        ],
      ];
    }
    return $elements;
  }

}
