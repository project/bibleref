<?php

namespace Drupal\bibleref\Formatter;

use Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceComplexInterface;
use Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Theme\ThemeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default formatter for bible verse reference fields.
 *
 * @package Drupal\bibleref\Formatter
 */
class BibleRefFormatter implements BibleRefFormatterInterface, ContainerInjectionInterface {

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Current language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected LanguageInterface $currentLang;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('theme.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * BibleRefFormatter constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   Theme manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    ThemeManagerInterface $theme_manager,
    LanguageManagerInterface $language_manager
  ) {
    $this->moduleHandler = $module_handler;
    $this->themeManager = $theme_manager;
    $this->currentLang = $language_manager->getCurrentLanguage();
  }

  /**
   * {@inheritdoc}
   */
  public function format(BibleReferenceInterface $item) {
    $args = [
      'format' => ':book :chapter.:verse',
      'values' => $item->toArray(),
      'args' => [
        ':book' => $this->getBookLabel($item),
        ':chapter' => $item->getChapter(),
        ':verse' => $this->formatVerse($item),
      ],
    ];
    /** @var \Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceComplex $item */
    if ($item instanceof BibleReferenceComplexInterface) {
      $args['args'] += [
        ':chapter_to' => $item->getChapterTo(),
        ':verse_to' => $item->getVerseTo(),
      ];
    }
    $args['format'] = $this->getFormat($item, $args['args']);

    $context = [
      'item' => $item,
    ];
    $this->alter('bibleref_format_data', $args, $context);
    // phpcs:ignore Drupal.Semantics.FunctionT.NotLiteralString
    return new TranslatableMarkup($args['format'], $args['args'], ['context' => 'bibleref']);
  }

  /**
   * Get book label.
   *
   * @param \Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface $item
   *   Item.
   *
   * @return string
   *   Book label to display.
   */
  protected function getBookLabel(BibleReferenceInterface $item): string {
    $term = $item->getBook();
    $label = $term->label();
    $langcode = $this->currentLang->getId();
    if ($term->hasTranslation($langcode)) {
      $label = $term->getTranslation($langcode)->label();
    }
    return $label;
  }

  /**
   * Get format string.
   *
   * @param \Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface $item
   *   Bible reference item.
   * @param array $args
   *   An associative array of replacements.
   *
   * @return string
   *   Format string.
   */
  protected function getFormat(BibleReferenceInterface $item, array $args): string {
    if (
      $item instanceof BibleReferenceComplexInterface
      && !empty($item->getChapterTo())
    ) {
      if ($item->getChapterTo() == $item->getChapter()) {
        return ':book :chapter,:verse-:verse_to';
      }
      if (empty($item->getVerseTo())) {
        return ':book :chapter,:verse-:chapter_to';
      }
      return ':book :chapter,:verse-:chapter_to,:verse_to';
    }

    if (
      $item->getAdditional()
      && empty($args[':chapter'])
    ) {
      return ':book :verse';
    }

    if (
      $item->getAdditional()
      && !$item->getVerseFrom()
    ) {
      return ':book :chapter :verse';
    }

    if (
      !$item->getAdditional()
      && empty($args[':verse'])
    ) {
      return ':book :chapter';
    }

    // Default format.
    return ':book :chapter.:verse';
  }

  /**
   * {@inheritdoc}
   */
  public function formatVerse(BibleReferenceInterface $item) {
    $values = $item->toArray();
    $args = [
      'format' => ':verse_from',
      'values' => [
        ':verse_from' => $values[BibleReferenceInterface::PROPERTY_VERSE_FROM] ?? '',
        ':verse_to' => $values[BibleReferenceInterface::PROPERTY_VERSE_TO] ?? '',
        ':verse_additional' => $values[BibleReferenceInterface::PROPERTY_VERSE_ADDITIONAL] ?? '',
      ],
    ];
    if (empty(array_filter($args['values']))) {
      return '';
    }

    if (
      !empty($values[BibleReferenceInterface::PROPERTY_VERSE_TO])
      && empty($values[BibleReferenceComplexInterface::PROPERTY_CHAPTER_TO])
    ) {
      $args['format'] = ':verse_from - :verse_to';
    }
    if (!empty($values[BibleReferenceInterface::PROPERTY_VERSE_ADDITIONAL])) {
      if (!empty($values[BibleReferenceInterface::PROPERTY_VERSE_FROM])) {
        $args['format'] .= '; :verse_additional';
      }
      else {
        $args['format'] = ':verse_additional';
      }
    }

    $context = [
      'item' => $item,
    ];
    $this->alter('bibleref_format_verse', $args, $context);
    // phpcs:ignore Drupal.Semantics.FunctionT.NotLiteralString
    return new TranslatableMarkup($args['format'], $args['values'], ['context' => 'bibleref']);
  }

  /**
   * Invoke alter hooks.
   */
  protected function alter($hook, &$data, &$context) {
    $this->moduleHandler->alter($hook, $data, $context);
    $this->themeManager->alter($hook, $data, $context);
  }

}
