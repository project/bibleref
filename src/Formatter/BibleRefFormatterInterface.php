<?php

namespace Drupal\bibleref\Formatter;

use Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface;

/**
 * Interface of formatter for bible verse reference fields.
 *
 * @package Drupal\bibleref\Formatter
 */
interface BibleRefFormatterInterface {

  /**
   * Format given reference.
   *
   * @param \Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface $item
   *   Bible reference item.
   *
   * @return string|\Drupal\Component\Render\MarkupInterface
   *   Formatted string.
   */
  public function format(BibleReferenceInterface $item);

  /**
   * Get verse part of reference item.
   *
   * @param \Drupal\bibleref\Plugin\Field\FieldType\BibleReferenceInterface $item
   *   Bible reference item.
   *
   * @return string|\Drupal\Component\Render\MarkupInterface
   *   Formatted string.
   */
  public function formatVerse(BibleReferenceInterface $item);

}
